#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <string>
#include <iostream>

using namespace std;
using namespace cv;


Point startP, endP;

int pressed = 0;

void CallBackFunc(int event, int x, int y, int flags, void* userdata)
{
	
	if (event == EVENT_LBUTTONDOWN)
	{
		cout << "Left button of the mouse is clicked - position (" << x << ", " << y << ")" << endl;
		pressed = 1;
		startP.x = x;
		startP.y = y;
	}
	else if (event == EVENT_RBUTTONDOWN)
	{
		cout << "Right button of the mouse is clicked - position (" << x << ", " << y << ")" << endl;
	}
	else if (event == EVENT_LBUTTONUP) {
		cout << "digo tipku" << endl;
		pressed = 0;
	}
	else if (event == EVENT_MBUTTONDOWN)
	{
		cout << "Middle button of the mouse is clicked - position (" << x << ", " << y << ")" << endl;
	}
	else if (event == EVENT_MOUSEMOVE)
	{
		cout << "Mouse move over the window - position (" << x << ", " << y << ")" << endl;
		if (pressed) {
			endP.x = x;
			endP.y = y;
		}
	}
}

int main(int argc, char** argv)
{
	Mat src, hsv;
	VideoCapture cap(0); // open the default camera
	if (!cap.isOpened())  // check if we succeeded
		return -1;

	string text = "Funny text inside the box";
	int fontFace = FONT_HERSHEY_SCRIPT_SIMPLEX;
	double fontScale = 1;
	int thickness = 3;



	namedWindow("Source", 1);
	while (1) {
		cap >> src;


		int baseline = 0;
		Size textSize = getTextSize(text, fontFace,
			fontScale, thickness, &baseline);
		baseline += thickness;


		rectangle(src,  Point(20,20), Point(100,100),
			Scalar(0, 0, 255), 3);

		rectangle(src, startP, endP,
			Scalar(0, 0, 255), 3);


		setMouseCallback("Source", CallBackFunc, NULL);
		
		imshow("Source", src);

		//waitKey(0);
		waitKey(10);
	}

}