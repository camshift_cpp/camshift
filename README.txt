Upute za pode�avanja OpenCV-a za Visual Studio:
1) instalirati Visual Studio 2015
2) skinuti i instalirati ovu verziju OpenCV-a (http://sourceforge.net/projects/opencvlibrary/files/opencv-win/3.0.0/opencv-3.0.0.exe/download)
3) u cmd upisati setx -m OPENCV_DIR <putanja do va�e opencv instalacije do foldera build/x64 ili x86/vc12>
4) odete na ThisPC, propertis, Advanced System Settings i kliknete na Environment Variables u donjoj sekciji kliknete na Path i edit, odete na kraj textbox-a, upi�ete ";%OPENCV_DIR%\bin"
5) potegnete s gita projekt

U VISUAL STUDIU NE MIJENJATI NIKAKVE POSTAVKE PROJEKTA!!!
Ako ne�to ne radi napi�ite u chat pa �emo rije�iti